# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions

# This is a simple example for a custom action which utters "Hello World!"

from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import requests
import json

from typing import Dict, Text, Any, List, Union, Optional

from rasa.shared.core.events import SlotSet
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.forms import FormValidationAction
from re import search


class ActionHelloWorld(Action):
    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Hello World! It is my first action")
        return []


class ActionGetNewst(Action):
    def name(self):
        return 'action_get_news'

    def run(self, dispatcher, tracker, domain):
        category = tracker.get_slot('category')
        if category == None:
            category = 'monde'
        print('category ', category)
        try:
            url = 'https://newsapi.org/v2/everything?q={category}&apiKey=5f839c4b07a44b5d9bf69baf9b1bc611'.format(
                category=category)
            response = requests.get(url).text
            # print('response',response)
            json_data = json.loads(response)['articles']
            i = 0
            # print('response articles ' ,response)
            for results in json_data:
                i = i + 1
                message = "<i>" + str(i) + "." + results['title'] + "</i>"
                print(i)
                dispatcher.utter_message(message)
                if i > 4:
                    return
        except Exception as e:
            print(e)
            dispatcher.utter_message(
                text=f"<br><b>custom action message</b><br>")
        return [SlotSet('category', category)]


class Telephone(Action):
    """example of custom action"""

    def name(self):
        """name of the custom action"""
        return "action_get_list_telephone"

    def run(self, dispatcher, tracker, domain):
        """ list from API"""
        headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
        url = 'http://d160cf45d334.ngrok.io/agent/getByCriteria'
        data = {
            "data": {}
        }

        result = requests.post(url, json=data, headers=headers)
        mydata = result.json()
        gt = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [

                    ]
                }
            }
        }
        usersdatas = []
        for item in mydata['items']:
            userdata = {
                "Nom": item['nom'],
                # "image_url":r['items'][i]['urlToImage'],
                "prenom": item['prenom'],
                "buttons": [
                    {
                        "type": "text",
                        "title": "Read More"
                    },
                ]
            }
            usersdatas.append(userdata)
        gt['attachment']['payload']['elements'] = usersdatas

        dispatcher.utter_message(gt)    
        return []


class ValidateRecuperationDuForm(FormValidationAction):
    """Example of a form validation action."""

    def name(self) -> Text:
        return "validate_recuperation_du_form"

    @staticmethod
    def is_int(string: Text) -> bool:
        """Check if a string is an integer."""

        try:
            int(string)
            return True
        except ValueError:
            return False

    def validate_nom_complet(
            self,
            value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validation du nom saisi par l'utilisateur"""

        jesuis = "je suis "
        jemappelle = "je m'appelle "
        jemenomme = "je me nomme "
        monnomcest="mon nom c'est "
        information =""
        nomaucasou=""
        if(search(jesuis, value.lower())):
            #spliter l information entre par utilisateur
            information = value.lower().split(jesuis)

        elif(search(jemappelle, value.lower())):
            #spliter l information entre par utilisateur
            information = value.lower().split(jemappelle)

        elif(search(jemenomme, value.lower())):
            #spliter l information entre par utilisateur
            information = value.lower().split(jemenomme) 
        
        elif(search(monnomcest, value.lower())):
            #spliter l information entre par utilisateur
            information = value.lower().split(monnomcest)
        elif(search(nomaucasou, value.lower())):
            #spliter l information entre par utilisateur
            information = value.lower().split(monnomcest)
            information.insert(0,"")
              
        res = information

        myintent = tracker.latest_message['intent'].get('name')
        
        if(myintent == "bonjour" or myintent == "salutations" ):
            return {"nom_complet": None}
        
        #print(res[1])
        return {"nom_complet": res[1]}
    
       
       
        #else:
            #sinon affichage du message d'erreur que l'age n'est pas valide
            #dispatcher.utter_message(template="utter_mauvais_nom")
            # attribution de valeur null au slot
            #return {"nom_complet": None}
            #return {"nom_complet": value}
   
class ValidateResponsableDuForm(FormValidationAction):
    """Example of a form validation action."""

    def name(self) -> Text:
        return "validate_responsable_du_form"

    @staticmethod
    def is_int(string: Text) -> bool:
        """Check if a string is an integer."""

        try:
            int(string)
            return True
        except ValueError:
            return False

    def validate_nom_respo(
            self,
            value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) ->  Dict[Text, Any]:
        """Validation du nom saisi par l'utilisateur"""
       
        try:
            b1 = "je veux aller au bureau de "
            b2 = "montre moi le chemin pour aller au bureau de "
            b3 = "où se trouve le bureau de "
            b4 = "conduitt moi au bureau de "
            b5 = "fais moi visiter "
            b6 = "je veux visiter "
           
            
            information =""

            if(search(b1, value.lower())):
                #spliter l information entre par utilisateur
                information = value.lower().split(b1) 
            elif(search(b2, value.lower())):
                #spliter l information entre par utilisateur
                information = value.lower().split(b2) 
            elif(search(b3, value.lower())):
                #spliter l information entre par utilisateur
                information = value.lower().split(b3) 
            elif(search(b4, value.lower())):
                    #spliter l information entre par utilisateur
                information = value.lower().split(b4) 
            elif(search(b5, value.lower())):
                    #spliter l information entre par utilisateur
                information = value.lower().split(b5) 
            elif(search(b6, value.lower())):
                    #spliter l information entre par utilisateur
                information = value.lower().split(b6) 
            else:
            #     #spliter l information entre par utilisateur
                dispatcher.utter_message(template="utter_mauvais_nom")
                return {"nom_respo": None}

            myintent = tracker.latest_message['intent'].get('name')
            
            if(myintent == "oscar" or myintent == "telo"):
                return {"nom_respo": None}

            nom_res = information[1]
            print("b:"+nom_res)
        
            if len(nom_res) > 0:
                #print("b:"+nom_res)
                
                return {"nom_respo": nom_res}
          
            else:
                dispatcher.utter_message(template="utter_mauvais_nom")
                 # attribution de valeur null au slot
                return {"nom_respo": None}
        except Exception as e:
            print("error")
            print(e)
            # dispatcher.utter_message(template="utter_mauvais_nom")
            return {"nom_respo": None}


class AppelApi(Action):
    
    def name(self) -> Text:
        return "action_appel_api"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    
        lieu=""
        # je recupere la valeur qui se trouve dans le slot de nom_respo 
        nom_responsable = tracker.get_slot("nom_respo")
        #print(nom_responsable)

        try:
            #url = 'https://newsapi.org/v2/everything?q=techcrunch&apiKey=5f839c4b07a44b5d9bf69baf9b1bc611'
        
            url = 'http://192.168.252.228:5000/users/odc'
            #url = 'http://192.168.252.228:5000/users/'+nom_responsable
            
            response = requests.get(url).text
            if(len(response) != 0):
                
           
              
                json_data = json.loads(response)
                i = 0
                print("merde1")
                print(json_data)
                #lieu = json_data["location"] 
                #print(lieu)
                #print("merde3")
                dispatcher.utter_template("utter_suivre", tracker)
                print("merde4")
                dispatcher.utter_template("utter_suivre_lieu", tracker, lieu_indique=lieu)
            else :
                print("merde5")
                dispatcher.utter_template("utter_lieu_non_trouver",tracker, nom_respo=nom_responsable)
                
                # print("desolé je ne sais pas ou se trouve le bureau de "+nom_responsable)
        except Exception as e:
            
            print(e)
            # dispatcher.utter_message(text=f"<br><b>custom action message</b><br>")
            #print("pret a afficher le dispatcher")
        
        #print("dispatcher ok, return")
        #return {"lieu_indique": lieu}
        return []